package dskraynov.indexator.structure;

import java.util.List;

import dskraynov.indexator.file.dto.JavaClass;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IndexatorTest {

    /**
     * Class by full name, if there are classes ABC and ABABAB, then request AB/ab/Ab/aB must return both, ABC only
     * first,
     * ABCD -- empty
     */
    @Test
    public void testFullClassNameSearch() {
        var ABCclass = new JavaClass("ABC", "ABC.java");
        var ABABABclass = new JavaClass("ABABAB", "ABABAB.java");
        List<JavaClass> classes = List.of(ABCclass, ABABABclass);
        IndexTrie index = new FullClassNameTrie(classes);
        var AB = index.getClasses("AB");
        assertEquals(2, AB.size());
        assertTrue(AB.containsAll(classes));
        var ab = index.getClasses("ab");
        assertEquals(2, ab.size());
        assertTrue(ab.containsAll(classes));
        var Ab = index.getClasses("Ab");
        assertEquals(2, Ab.size());
        assertTrue(Ab.containsAll(classes));
        var aB = index.getClasses("aB");
        assertEquals(2, aB.size());
        assertTrue(aB.containsAll(classes));

        var ABC = index.getClasses("ABC");
        assertEquals(1, ABC.size());
        assertTrue(ABC.contains(ABCclass));

        var ABCD = index.getClasses("ABCD");
        assertEquals(0, ABCD.size());
    }

    /**
     * Classes by capital letters, if there are classes MyLovelyClass and MyLovelySuperClass, then ML must produce
     * both, MLC only first, MLSC -- only second, MLD -- empty
     */
    @Test
    public void testCapitalLettersSearch() {
        var MyLovelyClassPath = new JavaClass("MyLovelyClass", "MyLovelyClass.java");
        var MyLovelySuperClassPath = new JavaClass("MyLovelySuperClass", "MyLovelySuperClass.java");
        var classes = List.of(MyLovelyClassPath, MyLovelySuperClassPath);
        IndexTrie index = new ClassCapitalLettersTrie(classes);

        var ML = index.getClasses("ML");
        assertEquals(2, ML.size());
        assertTrue(ML.containsAll(classes));

        var MLC = index.getClasses("MLC");
        assertEquals(1, MLC.size());
        assertTrue(MLC.contains(MyLovelyClassPath));

        var MLSC = index.getClasses("MLSC");
        assertEquals(1, MLSC.size());
        assertTrue(MLSC.contains(MyLovelySuperClassPath));

        var MLD = index.getClasses("MLD");
        assertEquals(0, MLD.size());
    }
}
