package dskraynov.indexator.structure;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import dskraynov.indexator.file.dto.JavaClass;

/**
 * Stores Java classes by capital letters and find all of them by capital letters only
 */
public class ClassCapitalLettersTrie extends IndexTrie {
    public ClassCapitalLettersTrie(List<JavaClass> allJavaFiles) {
        super(allJavaFiles);
    }

    public ClassCapitalLettersTrie(TrieNode root) {
        super(root);
    }

    @Override
    public void insert(JavaClass javaClass) {
        TrieNode current = root;

        for (char l : javaClass.getClassName().toCharArray()) {
            if (l >= 'A' && l <= 'Z') {
                current = current.getChildren().computeIfAbsent(l, c -> new TrieNode());
            }
        }
        current.addClassPath(javaClass);
    }

    @Override
    public void remove(JavaClass javaClass) {
        StringBuilder builder = new StringBuilder();
        for (char l : javaClass.getClassName().toCharArray()) {
            if (l >= 'A' && l <= 'Z') {
                builder.append(l);
            }
        }
        remove(root, builder.toString(), javaClass, 0);
    }

    private boolean remove(TrieNode current, String search, JavaClass javaClass, int index) {
        if (index == search.length()) {
            if (!current.containsClass(javaClass)) {
                return false;
            }
            current.remove(javaClass);
            return current.getChildren().isEmpty();
        }
        char ch = search.charAt(index);
        TrieNode node = current.getChildren().get(ch);
        if (node == null) {
            return false;
        }
        boolean shouldDeleteCurrentNode = remove(node, search, javaClass, index + 1) && node.getClasses().isEmpty();

        if (shouldDeleteCurrentNode) {
            current.getChildren().remove(ch);
            return current.getChildren().isEmpty();
        }
        return false;
    }

    @Override
    public Set<JavaClass> getClasses(String prefix) {
        TrieNode current = root;

        for (int i = 0; i < prefix.length(); i++) {
            char ch = prefix.charAt(i);
            TrieNode node = current.getChildren().get(ch);
            if (node == null) {
                return Collections.emptySet();
            }
            current = node;
        }
        return current.getAllChildClasses();
    }
}