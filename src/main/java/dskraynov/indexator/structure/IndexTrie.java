package dskraynov.indexator.structure;

import java.util.Collection;
import java.util.List;

import dskraynov.indexator.enums.Mode;
import dskraynov.indexator.file.dto.JavaClass;

/**
 * Abstract trie that is used for effective class names storing
 */
public abstract class IndexTrie {
    protected final TrieNode root;

    public IndexTrie(List<JavaClass> javaClasses) {
        this.root = new TrieNode();
        insertAll(javaClasses);
    }

    public IndexTrie(TrieNode root) {
        this.root = root;
    }

    public static IndexTrie from(Mode key, TrieNode value) {
        return switch (key) {
            case FULL_NAME -> new FullClassNameTrie(value);
            case CAPITAL_LETTERS -> new ClassCapitalLettersTrie(value);
            default -> throw new IllegalStateException("Unexpected value: " + key);
        };
    }

    public abstract Collection<JavaClass> getClasses(String prefix);

    public void insertAll(Collection<JavaClass> javaClasses) {
        javaClasses.forEach(this::insert);
    }

    protected abstract void insert(JavaClass javaClass);

    public TrieNode getData() {
        return root;
    }

    public void removeAll(Collection<JavaClass> classes) {
        classes.forEach(this::remove);
    }

    protected abstract void remove(JavaClass javaClass);
}
