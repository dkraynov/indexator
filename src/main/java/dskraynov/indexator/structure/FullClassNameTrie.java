package dskraynov.indexator.structure;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import dskraynov.indexator.file.dto.JavaClass;

/**
 * Stores Java classes by full class names and find all of them by prefix ignoring the case
 */
public class FullClassNameTrie extends IndexTrie {
    public FullClassNameTrie(List<JavaClass> allJavaFiles) {
        super(allJavaFiles);
    }

    public FullClassNameTrie(TrieNode root) {
        super(root);
    }

    @Override
    public void insert(JavaClass javaClass) {
        TrieNode current = root;

        for (char l : javaClass.getClassName().toLowerCase().toCharArray()) {
            current = current.getChildren().computeIfAbsent(l, c -> new TrieNode());
        }
        current.addClassPath(javaClass);
    }

    @Override
    public void remove(JavaClass javaClass) {
        remove(root, javaClass.getClassName().toLowerCase(), javaClass, 0);
    }

    private boolean remove(TrieNode current, String search, JavaClass javaClass, int index) {
        if (index == search.length()) {
            if (!current.containsClass(javaClass)) {
                return false;
            }
            current.remove(javaClass);
            return current.getChildren().isEmpty();
        }
        char ch = search.charAt(index);
        TrieNode node = current.getChildren().get(ch);
        if (node == null) {
            return false;
        }
        boolean shouldDeleteCurrentNode = remove(node, search, javaClass, index + 1) && node.getClasses().isEmpty();

        if (shouldDeleteCurrentNode) {
            current.getChildren().remove(ch);
            return current.getChildren().isEmpty();
        }
        return false;
    }

    @Override
    public Collection<JavaClass> getClasses(String prefix) {
        TrieNode current = root;
        prefix = prefix.toLowerCase();

        for (int i = 0; i < prefix.length(); i++) {
            char ch = prefix.charAt(i);
            TrieNode node = current.getChildren().get(ch);
            if (node == null) {
                return Collections.emptyList();
            }
            current = node;
        }
        return current.getAllChildClasses();
    }
}