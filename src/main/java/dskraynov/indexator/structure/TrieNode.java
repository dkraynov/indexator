package dskraynov.indexator.structure;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dskraynov.indexator.file.dto.JavaClass;
import lombok.Data;

@Data
public class TrieNode {
    private Map<Character, TrieNode> children = new HashMap<>();
    private Set<JavaClass> classes = new HashSet<>();

    public void addClassPath(JavaClass path) {
        classes.add(path);
    }

    public boolean containsClass(JavaClass javaClass) {
        return classes.contains(javaClass);
    }

    public void remove(JavaClass javaClass) {
        classes.remove(javaClass);
    }

    @JsonIgnore
    public Set<JavaClass> getAllChildClasses() {
        Set<JavaClass> result = new HashSet<>(classes);
        children.values().stream().flatMap(n -> n.getAllChildClasses().stream()).forEach(result::add);
        return result;
    }
}