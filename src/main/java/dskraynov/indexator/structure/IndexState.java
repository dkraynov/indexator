package dskraynov.indexator.structure;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dskraynov.indexator.enums.Mode;
import dskraynov.indexator.file.dto.JavaClass;
import dskraynov.indexator.file.dto.JavaFile;
import dskraynov.indexator.file.dto.JavaFileDeserializer;
import dskraynov.indexator.file.dto.JavaFileSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IndexState {
    @JsonSerialize(keyUsing = JavaFileSerializer.class)
    @JsonDeserialize(keyUsing = JavaFileDeserializer.class)
    private Map<JavaFile, List<JavaClass>> fileToClasses;
    private Map<Mode, TrieNode> index;
    private Integer hashCode;
}
