package dskraynov.indexator;

import java.util.Collection;
import java.util.Scanner;

import dskraynov.indexator.core.Indexator;
import dskraynov.indexator.enums.Mode;
import dskraynov.indexator.file.dto.JavaClass;

public class Main {
    public static void main(String[] args) {
        System.out.println(
                """
                                  
                          .___            .___     ____  ___       __               \s
                          |   | ____    __| _/____ \\   \\/  /____ _/  |_  ___________\s
                          |   |/    \\  / __ |/ __ \\ \\     /\\__  \\\\   __\\/  _ \\_  __ \\
                          |   |   |  \\/ /_/ \\  ___/ /     \\ / __ \\|  | (  <_> )  | \\/
                          |___|___|  /\\____ |\\___  >___/\\  (____  /__|  \\____/|__|  \s
                                   \\/      \\/    \\/      \\_/    \\/                  \s
                                          
                        """
        );
        Scanner in = new Scanner(System.in);
        Indexator indexator = new Indexator();
        printHelp();
        while (true) {
            Mode mode = getMode(in);
            if (mode == Mode.EXIT) {
                break;
            }
            System.out.println("Input a class name: ");
            String input = in.next();
            Collection<JavaClass> classes = indexator.getClasses(input, mode);
            System.out.println("Classes:");
            classes.forEach(Main::printJavaClass);
            System.out.println();
        }
        in.close();
        System.out.println("Exit...");
    }

    private static Mode getMode(Scanner in) {
        try {
            System.out.println("Input option: ");
            Mode mode = Mode.UNKNOWN;
            if (in.hasNext()) {
                mode = Mode.fromCode(Integer.parseInt(in.next()));
            }
            if (mode == Mode.UNKNOWN) {
                System.out.println("Wrong option. Please check help");
                printHelp();
                return getMode(in);
            }
            return mode;
        } catch (NumberFormatException ex) {
            System.out.println("Wrong input. Please check help");
            printHelp();
            return getMode(in);
        }
    }

    private static void printJavaClass(JavaClass javaClass) {
        System.out.printf("- %s (%s)\n", javaClass.getClassName(), javaClass.getFilePath());
    }

    private static void printHelp() {
        System.out.println(
                """
                        You can choose any option before each request:
                        1 - find classes by full names
                        2 - find classes by capital letters
                        3 - mixed mode (1 and 2)
                        0 - exit
                        """
        );
    }
}