package dskraynov.indexator.core;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import dskraynov.indexator.enums.Mode;
import dskraynov.indexator.file.dto.FileSystem;
import dskraynov.indexator.file.FilesHelper;
import dskraynov.indexator.file.dto.JavaClass;
import dskraynov.indexator.file.dto.JavaFile;
import dskraynov.indexator.structure.ClassCapitalLettersTrie;
import dskraynov.indexator.structure.FullClassNameTrie;
import dskraynov.indexator.structure.IndexState;
import dskraynov.indexator.structure.IndexTrie;
import dskraynov.indexator.structure.TrieNode;

/**
 * Class that is responsible for initializing, storing and rebuilding current index
 */
public class Indexator {
    private FileSystem currentFileSystem;
    private Map<JavaFile, List<JavaClass>> fileToClasses;
    private Map<Mode, IndexTrie> indexTries;

    public Indexator() {
        init();
    }

    private void init() {
        final Map<Mode, IndexTrie> indexes;
        Optional<IndexState> indexStateOpt = FilesHelper.restoreIndexState();
        if (indexStateOpt.isEmpty()) {
            System.out.println("Indexing...");
            FileSystem currentFileSystem = FilesHelper.getCurrentFileSystem();
            this.currentFileSystem = currentFileSystem;
            indexes = new HashMap<>();
            var fileToClasses = FilesHelper.parseJavaFiles(currentFileSystem.getJavaFiles());
            List<JavaClass> javaClasses = fileToClasses.values().stream().flatMap(List::stream).collect(Collectors.toList());
            indexes.put(Mode.FULL_NAME, new FullClassNameTrie(javaClasses));
            indexes.put(Mode.CAPITAL_LETTERS, new ClassCapitalLettersTrie(javaClasses));
            IndexState state = FilesHelper.saveIndexState(fileToClasses, indexes, currentFileSystem.getHashCode());
            System.out.println("Indexing completed");
            this.fileToClasses = state.getFileToClasses();
            this.indexTries = indexes;
        } else {
            System.out.println("Restoring index...");
            IndexState indexState = indexStateOpt.get();
            Map<Mode, TrieNode> indexData = indexState.getIndex();
            indexTries = indexData.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> IndexTrie.from(e.getKey(), e.getValue())));
            this.currentFileSystem = new FileSystem(indexState.getFileToClasses().keySet(), indexState.getHashCode());
            this.fileToClasses = indexState.getFileToClasses();
            updateIndexatorsIfFilesChanged();
            System.out.println("Restore completed");
        }
    }

    public Collection<JavaClass> getClasses(String input, Mode mode) {
        updateIndexatorsIfFilesChanged();
        return switch (mode) {
            case FULL_NAME -> indexTries.get(Mode.FULL_NAME).getClasses(input);
            case CAPITAL_LETTERS -> indexTries.get(Mode.CAPITAL_LETTERS).getClasses(input);
            case MIXED -> {
                Set<JavaClass> mixed = new HashSet<>();
                mixed.addAll(indexTries.get(Mode.FULL_NAME).getClasses(input));
                mixed.addAll(indexTries.get(Mode.CAPITAL_LETTERS).getClasses(input));
                yield mixed;
            }
            case EXIT, UNKNOWN -> throw new IllegalStateException("Unexpected value: " + mode);
        };
    }

    private void updateIndexatorsIfFilesChanged() {
        int currentHashCode = FilesHelper.calculateCurrentHashCode();
        boolean filesChanged = currentHashCode != currentFileSystem.getHashCode();
        if (filesChanged) {
            System.out.println("Files changed");
            updateChangedFiles();
        }
    }

    private void updateChangedFiles() {
        System.out.println("Indexing...");
        Map<JavaFile, List<JavaClass>> fileToClassesBefore = this.fileToClasses;
        FileSystem newFileSystem = FilesHelper.getCurrentFileSystem();
        Set<JavaFile> filesAfter = newFileSystem.getJavaFiles();
        Set<JavaFile> removedFiles = new HashSet<>(fileToClassesBefore.keySet());
        removedFiles.removeAll(filesAfter);
        Set<JavaFile> newOrUpdatedFiles = new HashSet<>(filesAfter);
        newOrUpdatedFiles.removeAll(fileToClassesBefore.keySet());

        List<JavaClass> classesToRemove = removedFiles.stream().flatMap(javaFile -> fileToClassesBefore.get(javaFile)
                .stream()).collect(Collectors.toList());
        Map<JavaFile, List<JavaClass>> parsedJavaFiles = FilesHelper.parseJavaFiles(newOrUpdatedFiles);
        List<JavaClass> classesToInsert = parsedJavaFiles
                .values().stream().flatMap(List::stream).collect(Collectors.toList());
        indexTries.forEach((mode, indexTrie) -> {
            indexTrie.removeAll(classesToRemove);
            indexTrie.insertAll(classesToInsert);
        });
        removedFiles.forEach(this.fileToClasses::remove);
        this.fileToClasses.putAll(parsedJavaFiles);
        FilesHelper.saveIndexState(this.fileToClasses, indexTries, newFileSystem.getHashCode());
        this.currentFileSystem = newFileSystem;
        System.out.println("Indexing completed");
    }
}
