package dskraynov.indexator.enums;

import java.util.stream.Stream;

public enum Mode {
    FULL_NAME(1),
    CAPITAL_LETTERS(2),
    MIXED(3),
    EXIT(0),
    UNKNOWN(-1);

    private final int modeCode;

    Mode(int modeCode) {
        this.modeCode = modeCode;
    }

    public static Mode fromCode(int modeCode) {
        return Stream.of(values()).filter(m -> m.modeCode == modeCode).findFirst().orElse(UNKNOWN);
    }
}
