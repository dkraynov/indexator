package dskraynov.indexator.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.databind.ObjectMapper;
import dskraynov.indexator.enums.Mode;
import dskraynov.indexator.file.dto.FileSystem;
import dskraynov.indexator.file.dto.JavaClass;
import dskraynov.indexator.file.dto.JavaFile;
import dskraynov.indexator.structure.IndexState;
import dskraynov.indexator.structure.IndexTrie;
import dskraynov.indexator.structure.TrieNode;
import org.apache.commons.io.FileUtils;

public class FilesHelper {

    private static final Pattern CLASS_PATTERN =
            Pattern.compile("(.*(public |private )?\\s*(static|final)?\\s*(class|interface|record|enum|@interface)\\s+(?<class>\\w+)\\s*(\\(.*\\))?\\s*((\\s*extends\\s+\\w+\\s*)|(\\s*implements\\s+\\w+( ,\\w+)*\\s*))*\\s*\\{)+");
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final File INDEX_ON_DISK = new File("./index.json");

    public static FileSystem getCurrentFileSystem() {
        return new FileSystem(getAllJavaFiles(), calculateCurrentHashCode());
    }

    public static Map<JavaFile, List<JavaClass>> parseJavaFiles(Set<JavaFile> allJavaFiles) {
        Map<JavaFile, List<JavaClass>> fileToClasses = new HashMap<>();
        float currentPercent = 0;
        float oneFilePercent = (float) 1 / allJavaFiles.size() * 100;
        float newPercent = 0;
        long startTime = System.currentTimeMillis();
        for (JavaFile javaFile : allJavaFiles) {
            List<JavaClass> classes = new ArrayList<>();
            try {
                File file = new File(javaFile.getFilePath());
                String fileContent = FileUtils.readFileToString(file, "utf-8");
                Matcher matcher = CLASS_PATTERN.matcher(fileContent);
                while (matcher.find()) {
                    classes.add(new JavaClass(matcher.group("class"), file.getPath()));
                }
            } catch (IOException e) {
                System.out.println("Failed to read file: " + javaFile.getFilePath());
            }
            fileToClasses.put(javaFile, classes);
            newPercent += oneFilePercent;
            if ((int) newPercent - (int) currentPercent >= 5) {
                System.out.print((int) newPercent + "%...");
                currentPercent = newPercent;
            }
        }
        System.out.println("\nProcessed in " + (float) (System.currentTimeMillis() - startTime) / 1000 + " sec");
        return fileToClasses;
    }

    public static int calculateCurrentHashCode() {
        return getAllJavaFiles().stream().map(JavaFile::getLastModified).collect(Collectors.toSet()).hashCode();
    }

    public static Set<JavaFile> getAllJavaFiles() {
        try (Stream<Path> walk = Files.walk(Paths.get("."))) {
            return walk
                    .filter(p -> !Files.isDirectory(p))
                    .filter(f -> f.toString().endsWith("java"))
                    .map(Path::toFile)
                    .map(f -> new JavaFile(f.getPath(), f.lastModified()))
                    .collect(Collectors.toSet());
        } catch (IOException e) {
            System.out.println("Failed to walk through the directory");
            return Collections.emptySet();
        }
    }

    public static IndexState saveIndexState(Map<JavaFile, List<JavaClass>> fileToClasses,
                                            Map<Mode, IndexTrie> indexes, Integer hashCode) {
        try {
            Map<Mode, TrieNode> trieNodeMap = indexes.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getData()));
            IndexState indexState = new IndexState(fileToClasses, trieNodeMap, hashCode);
            String index = MAPPER.writeValueAsString(indexState);
            FileUtils.writeStringToFile(INDEX_ON_DISK, index, "utf-8");
            return indexState;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Optional<IndexState> restoreIndexState() {
        if (!INDEX_ON_DISK.exists()) {
            return Optional.empty();
        }
        try {
            String fileContent = FileUtils.readFileToString(INDEX_ON_DISK, "utf-8");
            IndexState indexState = MAPPER.readValue(fileContent, IndexState.class);
            return Optional.of(indexState);
        } catch (IOException e) {
            System.out.println("Failed to restore index state");
            return Optional.empty();
        }
    }
}
