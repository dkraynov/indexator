package dskraynov.indexator.file.dto;

import java.util.Set;

import lombok.Data;

@Data
public class FileSystem {
    private final Set<JavaFile> javaFiles;
    private final int hashCode;
}
