package dskraynov.indexator.file.dto;

import java.io.IOException;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JavaFileDeserializer extends KeyDeserializer {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public JavaFile deserializeKey(String key, DeserializationContext ctxt) throws IOException {
        return mapper.readValue(key, JavaFile.class);
    }
}