# Indexator

## What is it?
Project that is intended to be an "indexator" - it scans current Java project and allows you to perform search classes with three types of requests:
- by prefix of full class name
- by capital letters
- mixed mode

## How to build
```shell
gradle clean build
```

## How to run
Put the jar file into the root folder of your Java project
```shell
java -jar Indexator-1.0.3.jar
```
You will see a welcome message with instructions.

## Example

Here is the example output of the program. It scans the current project.
```shell
  .___            .___     ____  ___       __                
  |   | ____    __| _/____ \   \/  /____ _/  |_  ___________ 
  |   |/    \  / __ |/ __ \ \     /\__  \\   __\/  _ \_  __ \
  |   |   |  \/ /_/ \  ___/ /     \ / __ \|  | (  <_> )  | \/
  |___|___|  /\____ |\___  >___/\  (____  /__|  \____/|__|   
           \/      \/    \/      \_/    \/                   


Restoring index...
Files changed
Indexing...
7%...14%...21%...28%...35%...42%...50%...57%...64%...71%...78%...85%...92%...100%...
Processed in 0.164 sec
Indexing completed
Restore completed
You can choose any option before each request:
1 - find classes by full names
2 - find classes by capital letters
3 - mixed mode (1 and 2)
0 - exit

Input option: 
1
Input a class name: 
Ind
Classes:
- Indexator (./src/main/java/dskraynov/indexator/core/Indexator.java)
- IndexState (./src/main/java/dskraynov/indexator/structure/IndexState.java)
- IndexatorTest (./src/test/java/dskraynov/indexator/structure/IndexatorTest.java)
- IndexTrie (./src/main/java/dskraynov/indexator/structure/IndexTrie.java)

Input option: 
2
Input a class name: 
IT
Classes:
- IndexatorTest (./src/test/java/dskraynov/indexator/structure/IndexatorTest.java)
- IndexTrie (./src/main/java/dskraynov/indexator/structure/IndexTrie.java)

Input option: 
0
Exit...
```